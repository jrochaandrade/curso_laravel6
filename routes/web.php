<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');



//Rota para alterar o dados do usuário
/* Route::get('/model', function(){
$user = App\User::find(1);
$user->name = 'Admin';
$user->email = 'admin@example.com';
$user->password = bcrypt('admin');
$user->save();
}); */

//Rota para listar todos os produtos
/* Route::get('/model', function(){
    $products = App\Product::all();

    return $products;
    
    });*/

//Rota para listar todos os produtos
/* Route::get('/model', function(){
    
    return App\User::all();
}); */

//Rota para listar todos os usuários paginados
/* Route::get('/model', function(){
    return \App\User::paginate(10);
}); */

/* Route::get('/model', function(){
    $user = App\User::find(4);
    return $user->store;
    return \App\User::all();
}); */

//Rota normal
/* Route::get('/admin/stores', 'Admin\\StoreController@index');
Route::get('/admin/stores/create', 'Admin\\StoreController@create');
Route::post('/admin/stores/store', 'Admin\\StoreController@store'); */


Route::group(['middleware' => ['auth']], function () 
{
    //Rotas simplificadas
    Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function () {

        /* Route::prefix('/stores')->name('stores.')->group(function () 
        {
            Route::get('/', 'StoreController@index')->name('index');
            Route::get('/create', 'StoreController@create')->name('create');
            Route::post('/store', 'StoreController@store')->name('store');
            Route::get('/{store}/edit', 'StoreController@edit')->name('edit');
            Route::post('/update/{store}', 'StoreController@update')->name('update');
            Route::get('/destroy/{store}', 'StoreController@destroy')->name('destroy');
        }); */
        
        Route::resource('stores', 'StoreController');
        Route::resource('products', 'ProductController');
        Route::resource('categories', 'CategoryController');

        Route::post('photos/remove', 'ProductPhotoController@removePhoto')->name('photo.remove');

    });    
});


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
