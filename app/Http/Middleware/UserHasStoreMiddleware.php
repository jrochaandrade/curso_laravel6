<?php

namespace App\Http\Middleware;

use Closure;

class UserHasStoreMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /* Criar a função para verificar se o usuário já possui uma loja e não carregar a pagina atraves do navegador */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->store){
            flash('Você já possui uma loja cadastrada!')->error();
            return redirect()->route('admin.stores.index');
        }

        return $next($request);
    }
    
}
