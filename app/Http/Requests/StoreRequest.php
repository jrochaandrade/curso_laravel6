<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /* Informações que obrigatóriamente precisam ser preenchida */
            'name' => 'required',
            'description' => 'required|min:10',
            'phone' => 'required',
            'mobile_phone' => 'required',
            'logo' => 'image'
        ];
    }

    /* Função para colocar mensagem de erro em português */
    public function messages()
    {
        return [
            /* 'required' => 'O campo :attribute é obrigatório', */ /* Coloca o nome do campo na tela tmb */
            'required' => 'Este campo é obrigatório',
            'min' => 'Este campo deve ter no mínimo :min caracteres',
            'image' => 'Arquivo não é uma imagem válida'
        ];
    }
}
