<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Marktplace L6</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
    <!-- CSN para adiconar estilização do bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <!-- CDN para adiconar icones ao site -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
</head>

<body>
    <!-- Adcionando o navbar -->
    <nav class="navbar navbar-expand-lg bg-body-tertiary" data-bs-theme="dark" style="margin-bottom: 40px;">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{route('home')}}">Marketplace L6</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                @auth <!--só exibe o conteúdo se o usuário estiver logado -->
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link @if(request()->is('admin/stores*')) active @endif" aria-current="page" href="{{ route('admin.stores.index') }}">Lojas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(request()->is('admin/products*')) active @endif" href="{{ route('admin.products.index') }}">Produtos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(request()->is('admin/categories*')) active @endif" href="{{ route('admin.categories.index') }}">Categorias</a>
                        </li>                    
                    </ul>
                    <div class="my-2 my-lg-0">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="#" onclick="event.preventDefault(); document.querySelector('form.logout').submit(); ">Sair</a>
                                <form action="{{route('logout')}}" class="logout" method="POST" style="display:none;">
                                    @csrf
                                </form>
                            </li>
                            <li>
                                <span class="nav-link">{{auth()->user()->name}}</span>
                            </li>
                        </ul>
                    </div>
                @endauth                
            </div>
        </div>
    </nav>
    <div class="container">
        @include('flash::message')
        @yield('content')
    </div>
</body>
</html>
