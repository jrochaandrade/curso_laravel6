@extends('layouts.app')
@section('content')

<!-- Para fazer o botão de criar loja só aparecer se o usuário não possuir loja -->
@if(!$store)
    <a href="{{ route('admin.stores.create') }}" class="btn btn-lg btn-success mt-3">Criar loja</a>
@else
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nome da loja</th>
            <th>Total de produtos</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>        
        <tr>
            <td>{{ $store->id  }}</td>
            <td>{{ $store->name }}</td>
            <td>{{ $store->products->count()}}</td>
            <td>
                <div class="btn-group">
                    <a href="{{ route('admin.stores.edit', ['store'=>$store->id]) }}" class="btn btn-sm btn-primary" title="Editar"><i class="bi bi-pencil-square"></i></a>
                    <form action="{{ route('admin.stores.destroy', ['store'=>$store->id]) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-sm btn-danger" title="Excluir"><i class="bi bi-trash3-fill"></i></button>
                    </form>
                </div>
            </td>
        </tr>        
    </tbody>
</table>
@endif
@endsection
