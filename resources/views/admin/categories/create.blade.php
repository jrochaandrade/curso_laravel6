@extends('layouts.app')
@section('content')
    <h1>Criar categoria</h1>
    <form action="{{ route('admin.categories.store') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label>Nome da categoria</label>
            <!-- Identifica o erro e retorna na tela para o usuário no imput -->
            <!-- old retorna no input o ultimo texto digitado pelo usuário -->
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}">            
            @error('name') <!-- Identifica o erro e retorna na tela para o usuário -->
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Descrição</label>
            <input type="text" name="description" class="form-control @error('description') is-invalid @enderror" value="{{old('description')}}">
            @error('description')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
            @enderror
        </div>        
        <div class="form-group">
            <label>Slug</label>
            <input type="text" name="slug" class="form-control">
        </div>                
        <div>
            <button type="submit" class="btn btn-lg btn-success mt-3">Criar categoria</button>
            <a href="{{ route('admin.categories.index') }}" class="btn btn-lg btn-secondary mt-3">Voltar</a>
        </div>        
    </form>
@endsection
