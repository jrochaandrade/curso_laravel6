@extends('layouts.app')
@section('content')

<a href="{{ route('admin.categories.create') }}" class="btn btn-lg btn-success mt-3">Criar Categoria</a>

<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>            
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)        
            <tr>
                <td>{{ $category->id  }}</td>
                <td>{{ $category->name }}</td>            
                <td>
                    <div class="btn-group">
                        <a href="{{ route('admin.categories.edit', ['category'=>$category->id]) }}" class="btn btn-sm btn-primary" title="Editar"><i class="bi bi-pencil-square"></i></a>
                        <form action="{{ route('admin.categories.destroy', ['category'=>$category->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger" title="Excluir"><i class="bi bi-trash3-fill"></i></button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach        
    </tbody>
</table>

{{ $categories->links() }}

@endsection
